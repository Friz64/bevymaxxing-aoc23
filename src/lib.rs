use bevy::prelude::*;
use std::panic::Location;

#[track_caller]
pub fn configure_app(app: &mut App, window: bool) {
    let title = format!(
        "bevymaxxing advent of code 2023: {}",
        Location::caller().file()
    );

    if window {
        app.insert_resource(ClearColor(Color::rgb_u8(36, 36, 36)))
            .add_plugins(DefaultPlugins.set(WindowPlugin {
                primary_window: Some(Window {
                    title,
                    ..Default::default()
                }),
                ..Default::default()
            }));
    } else {
        println!("{title}");
        app.add_plugins(MinimalPlugins);
    }
}
