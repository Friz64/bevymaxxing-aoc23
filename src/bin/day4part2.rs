use bevy::{app::AppExit, prelude::*};

const INPUT: &str = include_str!("day4.txt");

fn main() {
    let mut app = App::new();
    bevymaxxing_aoc23::configure_app(&mut app, false);
    app.add_systems(Startup, input)
        .add_systems(Update, step)
        .add_systems(
            Update,
            output.run_if(|iteration: Res<Iteration>| iteration.current == iteration.total),
        )
        .run();
}

#[derive(Component)]
struct Scratchcard(usize);

#[derive(Component, Clone)]
struct WinningNumber(u32);

#[derive(Component, Clone)]
struct NumberIHave(u32);

#[derive(Resource)]
struct Iteration {
    current: usize,
    total: usize,
}

fn input(mut commands: Commands) {
    let mut scratchcards = 0;
    for scratchcard in INPUT.lines() {
        let (card, numbers) = scratchcard.split_once(':').unwrap();
        let card_number = card.trim_start_matches("Card").trim().parse().unwrap();
        commands
            .spawn(Scratchcard(card_number))
            .with_children(|child_builder| {
                let (winning_numbers, numbers_i_have) = numbers.split_once('|').unwrap();
                for winning_number in winning_numbers.split_whitespace() {
                    child_builder.spawn(WinningNumber(winning_number.parse().unwrap()));
                }

                for number_i_have in numbers_i_have.split_whitespace() {
                    child_builder.spawn(NumberIHave(number_i_have.parse().unwrap()));
                }
            });
        scratchcards += 1;
    }

    commands.insert_resource(Iteration {
        current: 1,
        total: scratchcards,
    });
}

fn step(
    mut commands: Commands,
    mut iteration: ResMut<Iteration>,
    scratchcards: Query<(&Scratchcard, &Children)>,
    winning_numbers: Query<&WinningNumber>,
    numbers_i_have: Query<&NumberIHave>,
) {
    println!("iteration {}", iteration.current);
    for (Scratchcard(current_num), children) in scratchcards
        .iter()
        .filter(|(Scratchcard(num), _children)| *num == iteration.current)
    {
        let applicable_winning_numbers: Vec<_> = children
            .iter()
            .filter_map(|entity| winning_numbers.get(*entity).ok())
            .map(|WinningNumber(num)| *num)
            .collect();

        let winning_numbers_i_have = children
            .iter()
            .filter_map(|entity| numbers_i_have.get(*entity).ok())
            .filter(|NumberIHave(num)| applicable_winning_numbers.contains(num))
            .count();

        for i in 0..winning_numbers_i_have {
            // clone the entity
            let new_num = current_num + 1 + i;
            commands
                .spawn(Scratchcard(new_num))
                .with_children(|child_builder| {
                    let (_scratchcard, children) = scratchcards
                        .iter()
                        .find(|(Scratchcard(num), _children)| *num == new_num)
                        .unwrap();
                    for &child in children {
                        if let Ok(winning_number) = winning_numbers.get(child) {
                            child_builder.spawn(winning_number.clone());
                        } else if let Ok(number_i_have) = numbers_i_have.get(child) {
                            child_builder.spawn(number_i_have.clone());
                        }
                    }
                });
        }
    }

    iteration.current += 1;
}

fn output(
    scratchcards: Query<(), With<Scratchcard>>,
    mut app_exit_events: ResMut<Events<AppExit>>,
) {
    let output = scratchcards.iter().count();
    println!("{output}");

    app_exit_events.send(AppExit);
}
