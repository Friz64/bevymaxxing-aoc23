use bevy::{app::AppExit, prelude::*};

const INPUT: &str = include_str!("day4.txt");

fn main() {
    let mut app = App::new();
    bevymaxxing_aoc23::configure_app(&mut app, false);
    app.add_systems(Startup, input)
        .add_systems(Update, calculate_worth)
        .add_systems(Update, output.after(calculate_worth))
        .run();
}

#[derive(Component)]
struct Scratchcard;

#[derive(Component, Default)]
struct Worth(Option<u32>);

#[derive(Component)]
struct WinningNumber(u32);

#[derive(Component)]
struct NumberIHave(u32);

fn input(mut commands: Commands) {
    for scratchcard in INPUT.lines() {
        let (_, numbers) = scratchcard.split_once(':').unwrap();
        commands
            .spawn((Scratchcard, Worth::default()))
            .with_children(|child_builder| {
                let (winning_numbers, numbers_i_have) = numbers.split_once('|').unwrap();
                for winning_number in winning_numbers.split_whitespace() {
                    child_builder.spawn(WinningNumber(winning_number.parse().unwrap()));
                }

                for number_i_have in numbers_i_have.split_whitespace() {
                    child_builder.spawn(NumberIHave(number_i_have.parse().unwrap()));
                }
            });
    }
}

fn calculate_worth(
    mut scratchcards: Query<(&mut Worth, &Children), With<Scratchcard>>,
    winning_numbers: Query<&WinningNumber>,
    numbers_i_have: Query<&NumberIHave>,
) {
    scratchcards
        .par_iter_mut()
        .for_each(|(mut worth, children)| {
            let winning_numbers: Vec<_> = children
                .iter()
                .filter_map(|entity| winning_numbers.get(*entity).ok())
                .map(|WinningNumber(num)| *num)
                .collect();

            let winning_numbers_i_have = children
                .iter()
                .filter_map(|entity| numbers_i_have.get(*entity).ok())
                .filter(|NumberIHave(num)| winning_numbers.contains(num))
                .count();

            worth.0 = Some(
                winning_numbers_i_have
                    .checked_sub(1)
                    .map(|n| 2u32.pow(n as u32))
                    .unwrap_or(0),
            );
        });
}

fn output(
    scratchcards: Query<&mut Worth, With<Scratchcard>>,
    mut app_exit_events: ResMut<Events<AppExit>>,
) {
    let output: u32 = scratchcards.iter().map(|worth| worth.0.unwrap()).sum();
    println!("{output}");

    app_exit_events.send(AppExit);
}
