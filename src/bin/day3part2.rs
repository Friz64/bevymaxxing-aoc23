use bevy::{app::AppExit, prelude::*};

const INPUT: &str = include_str!("day3.txt");

fn main() {
    let mut app = App::new();
    bevymaxxing_aoc23::configure_app(&mut app, false);
    app.add_systems(Startup, input)
        .add_systems(Update, determine_adjacent_numbers)
        .add_systems(Update, output.after(determine_adjacent_numbers))
        .run();
}

#[derive(Component)]
struct Position(UVec2);

#[derive(Component)]
struct Number {
    value: u32,
    length: usize,
}

#[derive(Component)]
struct Symbol(char);

#[derive(Component, Default)]
struct AdjacentNumbers(Option<Vec<Entity>>);

fn input(mut commands: Commands) {
    for (y, line) in INPUT.lines().enumerate() {
        let y = y as u32;
        let mut buf = String::new();
        let mut iter = line.chars().enumerate().peekable();
        while let (Some((x, c)), peek) = (iter.next(), iter.peek()) {
            if c.is_ascii_digit() {
                buf.push(c);
                if peek.is_none() || matches!(peek, Some((_x, c)) if !c.is_ascii_digit()) {
                    commands.spawn((
                        Position(UVec2::new(x as u32 - buf.len() as u32 + 1, y)),
                        Number {
                            value: buf.parse().unwrap(),
                            length: buf.len(),
                        },
                    ));

                    buf.clear();
                }
            } else if c != '.' {
                commands.spawn((
                    Position(UVec2 { x: x as u32, y }),
                    Symbol(c),
                    AdjacentNumbers::default(),
                ));
            }
        }
    }
}

fn determine_adjacent_numbers(
    numbers: Query<(Entity, &Number, &Position)>,
    mut symbols: Query<(&mut AdjacentNumbers, &Position), With<Symbol>>,
) {
    symbols
        .par_iter_mut()
        .for_each(|(mut adjacent_numbers, Position(symbol_position))| {
            let value = numbers
                .iter()
                .filter_map(|(number_entity, number, Position(number_position))| {
                    let is_adjacent = (symbol_position.y.abs_diff(number_position.y) <= 1)
                        && (symbol_position.x as i32 + 1 >= number_position.x as i32)
                        && (symbol_position.x as i32
                            <= number_position.x as i32 + number.length as i32);
                    is_adjacent.then_some(number_entity)
                })
                .collect();
            adjacent_numbers.0 = Some(value);
        });
}

fn output(
    symbols: Query<(&Symbol, &AdjacentNumbers)>,
    numbers: Query<&Number>,
    mut app_exit_events: ResMut<Events<AppExit>>,
) {
    let output: u32 = symbols
        .iter()
        .filter(|(Symbol(symbol), _adjacent_numbers)| *symbol == '*')
        .filter_map(|(Symbol(symbol), AdjacentNumbers(adjacent_numbers))| {
            let adjacent_numbers = adjacent_numbers.as_ref().unwrap();
            (*symbol == '*' && adjacent_numbers.len() == 2).then_some(adjacent_numbers)
        })
        .map(|adjacent_numbers| {
            adjacent_numbers
                .iter()
                .map(|&entity| numbers.get(entity).unwrap().value)
                .product::<u32>()
        })
        .sum();
    println!("{output}");

    app_exit_events.send(AppExit);
}
