use bevy::{app::AppExit, prelude::*};
use std::cmp::Ordering;

const INPUT: &str = include_str!("day1.txt");

fn main() {
    let mut app = App::new();
    bevymaxxing_aoc23::configure_app(&mut app, false);
    app.add_systems(Startup, input)
        .add_systems(Update, calculate_calibration_value)
        .add_systems(Update, output.after(calculate_calibration_value))
        .run();
}

#[derive(Component)]
struct CalibrationDocumentLine(&'static str);

#[derive(Default, Component)]
struct CalibrationValue(Option<u32>);

fn input(mut commands: Commands) {
    for line in INPUT.lines() {
        commands.spawn((CalibrationDocumentLine(line), CalibrationValue::default()));
    }
}

const SPELLED_OUT: &[&str] = &[
    "one", "two", "three", "four", "five", "six", "seven", "eight", "nine",
];

fn find_digit(line: &str, reverse: bool) -> u32 {
    let mut chars_iter = line.chars().collect::<Vec<_>>().into_iter();
    let digit_position = if reverse {
        chars_iter.rposition(|c| c.is_ascii_digit())
    } else {
        chars_iter.position(|c| c.is_ascii_digit())
    };

    let mut current =
        digit_position.map(|pos| (pos, line.chars().nth(pos).unwrap() as u32 - '0' as u32));

    let ordering = if reverse {
        Ordering::Greater
    } else {
        Ordering::Less
    };

    for (i, new_pos) in SPELLED_OUT
        .iter()
        .enumerate()
        .filter_map(|(i, s)| if reverse { line.rfind(s) } else { line.find(s) }.map(|pos| (i, pos)))
    {
        let new_value = i as u32 + 1;
        if current
            .map(|(pos, _value)| new_pos.cmp(&pos) == ordering)
            .unwrap_or(true)
        {
            current = Some((new_pos, new_value));
        }
    }

    current.unwrap().1
}

fn calculate_calibration_value(
    mut lines: Query<(&CalibrationDocumentLine, &mut CalibrationValue)>,
) {
    lines.par_iter_mut().for_each(|(line, mut value)| {
        let first = find_digit(line.0, false);
        let last = find_digit(line.0, true);
        value.0 = Some(first * 10 + last);
    });
}

fn output(lines: Query<&CalibrationValue>, mut app_exit_events: ResMut<Events<AppExit>>) {
    let output: u32 = lines.iter().map(|value| value.0.unwrap()).sum();
    println!("{output}");

    app_exit_events.send(AppExit);
}
