use bevy::{app::AppExit, prelude::*};

const INPUT: &str = include_str!("day2.txt");

fn main() {
    let mut app = App::new();
    bevymaxxing_aoc23::configure_app(&mut app, false);
    app.add_systems(Startup, input)
        .add_systems(Update, determine_possibility)
        .add_systems(Update, output.after(determine_possibility))
        .run();
}

const AVAILABLE: Set = Set {
    red: 12,
    green: 13,
    blue: 14,
};

#[derive(Component)]
struct Game(u32);

#[derive(Component, Default)]
struct Possible(Option<bool>);

#[derive(Component, Default)]
struct Set {
    red: u32,
    green: u32,
    blue: u32,
}

fn input(mut commands: Commands) {
    for line in INPUT.lines() {
        let (game_text, sets_text) = line.split_once(": ").unwrap();
        let game_id = game_text.trim_start_matches("Game ").parse().unwrap();
        commands
            .spawn((Game(game_id), Possible::default()))
            .with_children(|child_builder| {
                for set_text in sets_text.split("; ") {
                    let mut set = Set::default();
                    for cubes_text in set_text.split(", ") {
                        let (count_text, color) = cubes_text.split_once(' ').unwrap();
                        let count = count_text.parse().unwrap();
                        match color {
                            "red" => set.red = count,
                            "green" => set.green = count,
                            "blue" => set.blue = count,
                            _ => panic!(),
                        }
                    }

                    child_builder.spawn(set);
                }
            });
    }
}

fn determine_possibility(
    mut games: Query<(&mut Possible, &Children), With<Game>>,
    sets: Query<&Set>,
) {
    games.par_iter_mut().for_each(|(mut possible, children)| {
        possible.0 = Some(children.iter().all(|&child| {
            let set = sets.get(child).unwrap();
            set.red <= AVAILABLE.red && set.green <= AVAILABLE.green && set.blue <= AVAILABLE.blue
        }));
    });
}

fn output(games: Query<(&Game, &Possible)>, mut app_exit_events: ResMut<Events<AppExit>>) {
    let output: u32 = games
        .iter()
        .filter_map(|(game, possible)| (possible.0 == Some(true)).then_some(game.0))
        .sum();
    println!("{output}");

    app_exit_events.send(AppExit);
}
