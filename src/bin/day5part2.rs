use bevy::{app::AppExit, prelude::*};
use rayon::iter::{IntoParallelRefMutIterator, ParallelIterator};

const INPUT: &str = include_str!("day5.txt");

fn main() {
    let mut app = App::new();
    bevymaxxing_aoc23::configure_app(&mut app, false);
    app.add_systems(Startup, input)
        .add_systems(Update, step)
        .add_systems(
            Update,
            output
                .after(step)
                .run_if(|state: Res<State>| state.category == "location"),
        )
        .run();
}

#[derive(Component)]
struct Map {
    from: String,
    to: String,
}

#[derive(Component)]
struct Range {
    dst: u64,
    src: u64,
    len: u64,
}

#[derive(Resource)]
struct State {
    numbers: Vec<u64>,
    category: String,
}

fn input(mut commands: Commands) {
    let mut lines = INPUT.lines();

    let mut input_values = lines
        .next()
        .unwrap()
        .strip_prefix("seeds:")
        .unwrap()
        .split_whitespace()
        .map(|s| s.parse::<u64>().unwrap());

    let mut numbers = Vec::new();
    while let (Some(start), Some(len)) = (input_values.next(), input_values.next()) {
        for number in (start..).take(len as usize) {
            numbers.push(number);
        }
    }

    commands.insert_resource(State {
        numbers,
        category: "seed".into(),
    });

    let mut map_entity = None;
    for line in lines {
        match line {
            line if line.contains("map") => {
                let (from, to) = line
                    .split_whitespace()
                    .next()
                    .unwrap()
                    .split_once("-to-")
                    .unwrap();

                map_entity = Some(commands.spawn(Map {
                    from: from.into(),
                    to: to.into(),
                }));
            }
            "" => (),
            line => {
                let mut parts = line.split_whitespace().map(|s| s.parse().unwrap());
                map_entity.as_mut().unwrap().with_children(|child_builder| {
                    child_builder.spawn(Range {
                        dst: parts.next().unwrap(),
                        src: parts.next().unwrap(),
                        len: parts.next().unwrap(),
                    });
                });
            }
        }
    }
}

fn step(mut state: ResMut<State>, maps: Query<(&Map, &Children)>, ranges: Query<&Range>) {
    let (Map { to, .. }, children) = maps
        .iter()
        .find(|(map, _children)| map.from == state.category)
        .unwrap();

    let child_ranges: Vec<_> = children
        .iter()
        .map(|child| ranges.get(*child).unwrap())
        .collect();
    state.numbers.par_iter_mut().for_each(|number| {
        for range in &child_ranges {
            if *number >= range.src && *number < range.src + range.len {
                *number = (*number as i64 + (range.dst as i64 - range.src as i64)) as u64;
                break;
            }
        }
    });

    state.category = to.clone();
}

fn output(state: Res<State>, mut app_exit_events: ResMut<Events<AppExit>>) {
    let output = state.numbers.iter().min().unwrap();
    println!("{output}");

    app_exit_events.send(AppExit);
}
