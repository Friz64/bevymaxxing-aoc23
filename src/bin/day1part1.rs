use bevy::{app::AppExit, prelude::*};

const INPUT: &str = include_str!("day1.txt");

fn main() {
    let mut app = App::new();
    bevymaxxing_aoc23::configure_app(&mut app, false);
    app.add_systems(Startup, input)
        .add_systems(Update, calculate_calibration_value)
        .add_systems(Update, output.after(calculate_calibration_value))
        .run();
}

#[derive(Component)]
struct CalibrationDocumentLine(&'static str);

#[derive(Default, Component)]
struct CalibrationValue(Option<u32>);

fn input(mut commands: Commands) {
    for line in INPUT.lines() {
        commands.spawn((CalibrationDocumentLine(line), CalibrationValue::default()));
    }
}

fn calculate_calibration_value(
    mut lines: Query<(&CalibrationDocumentLine, &mut CalibrationValue)>,
) {
    lines.par_iter_mut().for_each(|(line, mut value)| {
        let first_digit = line.0.chars().find(char::is_ascii_digit).unwrap() as u32 - '0' as u32;
        let last_digit = line.0.chars().rfind(char::is_ascii_digit).unwrap() as u32 - '0' as u32;
        value.0 = Some(first_digit * 10 + last_digit);
    });
}

fn output(lines: Query<&CalibrationValue>, mut app_exit_events: ResMut<Events<AppExit>>) {
    let output: u32 = lines.iter().map(|value| value.0.unwrap()).sum();
    println!("{output}");

    app_exit_events.send(AppExit);
}
