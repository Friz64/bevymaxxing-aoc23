use bevy::{app::AppExit, prelude::*};

const INPUT: &str = include_str!("day2.txt");

fn main() {
    let mut app = App::new();
    bevymaxxing_aoc23::configure_app(&mut app, false);
    app.add_systems(Startup, input)
        .add_systems(Update, determine_power)
        .add_systems(Update, output.after(determine_power))
        .run();
}

#[derive(Component)]
struct Game(u32);

#[derive(Component, Default)]
struct Power(Option<u32>);

#[derive(Component, Default)]
struct Set {
    red: u32,
    green: u32,
    blue: u32,
}

fn input(mut commands: Commands) {
    for line in INPUT.lines() {
        let (game_text, sets_text) = line.split_once(": ").unwrap();
        let game_id = game_text.trim_start_matches("Game ").parse().unwrap();
        commands
            .spawn((Game(game_id), Power::default()))
            .with_children(|child_builder| {
                for set_text in sets_text.split("; ") {
                    let mut set = Set::default();
                    for cubes_text in set_text.split(", ") {
                        let (count_text, color) = cubes_text.split_once(' ').unwrap();
                        let count = count_text.parse().unwrap();
                        match color {
                            "red" => set.red = count,
                            "green" => set.green = count,
                            "blue" => set.blue = count,
                            _ => panic!(),
                        }
                    }

                    child_builder.spawn(set);
                }
            });
    }
}

fn determine_power(mut games: Query<(&mut Power, &Children), With<Game>>, sets: Query<&Set>) {
    games.par_iter_mut().for_each(|(mut power, children)| {
        let mut maximum_set = Set::default();
        for &child in children {
            let set = sets.get(child).unwrap();
            maximum_set.red = maximum_set.red.max(set.red);
            maximum_set.green = maximum_set.green.max(set.green);
            maximum_set.blue = maximum_set.blue.max(set.blue);
        }

        power.0 = Some(maximum_set.red * maximum_set.green * maximum_set.blue);
    });
}

fn output(games: Query<&Power, With<Game>>, mut app_exit_events: ResMut<Events<AppExit>>) {
    let output: u32 = games.iter().map(|power| power.0.unwrap()).sum();
    println!("{output}");

    app_exit_events.send(AppExit);
}
