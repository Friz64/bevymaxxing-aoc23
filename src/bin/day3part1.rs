use bevy::{app::AppExit, prelude::*};

const INPUT: &str = include_str!("day3.txt");

fn main() {
    let mut app = App::new();
    bevymaxxing_aoc23::configure_app(&mut app, false);
    app.add_systems(Startup, input)
        .add_systems(Update, determine_part_numbers)
        .add_systems(Update, output.after(determine_part_numbers))
        .run();
}

#[derive(Component)]
struct Position(UVec2);

#[derive(Component)]
struct Number {
    value: u32,
    length: usize,
}

#[derive(Component, Default)]
struct PartNumber(Option<bool>);

#[derive(Component)]
struct Symbol;

fn input(mut commands: Commands) {
    for (y, line) in INPUT.lines().enumerate() {
        let y = y as u32;
        let mut buf = String::new();
        let mut iter = line.chars().enumerate().peekable();
        while let (Some((x, c)), peek) = (iter.next(), iter.peek()) {
            if c.is_ascii_digit() {
                buf.push(c);
                if peek.is_none() || matches!(peek, Some((_x, c)) if !c.is_ascii_digit()) {
                    commands.spawn((
                        Position(UVec2::new(x as u32 - buf.len() as u32 + 1, y)),
                        PartNumber::default(),
                        Number {
                            value: buf.parse().unwrap(),
                            length: buf.len(),
                        },
                    ));

                    buf.clear();
                }
            } else if c != '.' {
                commands.spawn((Position(UVec2 { x: x as u32, y }), Symbol));
            }
        }
    }
}

fn determine_part_numbers(
    mut numbers: Query<(&Number, &Position, &mut PartNumber)>,
    symbols: Query<&Position, With<Symbol>>,
) {
    numbers
        .par_iter_mut()
        .for_each(|(number, Position(number_position), mut part_number)| {
            part_number.0 = Some(symbols.iter().any(|Position(symbol_position)| {
                (symbol_position.y.abs_diff(number_position.y) <= 1)
                    && (symbol_position.x as i32 + 1 >= number_position.x as i32)
                    && (symbol_position.x as i32 <= number_position.x as i32 + number.length as i32)
            }));
        });
}

fn output(numbers: Query<(&Number, &PartNumber)>, mut app_exit_events: ResMut<Events<AppExit>>) {
    let output: u32 = numbers
        .iter()
        .filter_map(|(number, PartNumber(part_number))| {
            part_number.unwrap().then_some(number.value)
        })
        .sum();
    println!("{output}");

    app_exit_events.send(AppExit);
}
